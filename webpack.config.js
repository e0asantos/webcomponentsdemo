const path = require("path");

const webpack = require("webpack");

// Plugins
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  resolve: {
    extensions: [".js", ".jsx", ".ts", ".tsx", ".json", ".scss", ".sass", ".ttf"],
  },

  entry: path.resolve(__dirname, "source", "index.tsx"),

  output: {
    path: path.resolve(__dirname, "build"),
    filename: "bundle.js",
  },

  module: {
    rules: [
      // linter preloader
      {
        test: /\.ts$/,
        enforce: "pre",
        loader: "tslint-loader",
        options: { typeCheck: true },
      },
      {
        test: /\.tsx$/,
        enforce: "pre",
        loader: "tslint-loader",
        options: { typeCheck: true },
      },

      // code
      { test: /\.js$/, use: ["ts-loader"], exclude: [/node_modules/] },
      { test: /\.jsx$/, use: ["ts-loader"], exclude: [/node_modules/] },
      { test: /\.ts$/, use: ["ts-loader"], exclude: [/node_modules/] },
      { test: /\.tsx$/, use: ["ts-loader"], exclude: [/node_modules/] },

      // styles
      {
        test: /\.css$|.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: [
            {
              loader: "style-loader",
            },
          ],
          use: [
            {
              loader: "css-loader",
              options: {
                modules: true,
                importLoaders: 2,
                localIdentName: "[local]___[hash:base64:5]",
              },
            },
            {
              loader: "sass-loader",
            },
          ],
        }),
      },

      // fonts
      { test: /\.(ttf|eot|woff|svg)$/, use: ["file-loader"], exclude: [/node_modules/] },

      // other assets
      { test: /\.json$/, use: ["json-loader"], exclude: [/node_modules/] },
    ],
  },

  plugins: [new ExtractTextPlugin({ filename: "styles.css" })],
};
