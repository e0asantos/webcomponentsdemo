import * as React from "react";
import { render } from "react-dom";

import { DemoApp } from "./DemoApp/DemoApp";

render(<DemoApp />, document.getElementById("root"));
