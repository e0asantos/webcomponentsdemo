/**
 * Registers a custom element
 * @param name Name of the custom element
 */
export function customElement(name: string): ClassDecorator {
  return function<T extends Function>(target: T): void {
    customElements.define(name, target);
  };
}
