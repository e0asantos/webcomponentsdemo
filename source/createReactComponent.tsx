import * as React from "react";

// tslint:disable-next-line:no-any
type IndexSignature = { [key: string]: any };

/**
 * Creates a React component from a custom element constructor
 * @param name Name of the custom element
 */
export function createReactComponent<TProps>(name: string): React.ComponentClass<TProps> {
  return class extends React.Component<TProps> {
    public static displayName: string = name;

    protected element: (Element & IndexSignature) | undefined;

    protected refHandler = (element: (Element & IndexSignature) | undefined): void => {
      if (element == undefined) {
        return;
      }

      this.element = element;

      // quickly fill newly created element properties with props - no comparison needed
      for (const key of Object.keys(this.props)) {
        if (key === "children") {
          return;
        }

        this.element[key] = (this.props as TProps & IndexSignature)[key];
      }
    };

    public componentWillReceiveProps(nextProps: TProps & IndexSignature): void {
      if (this.element == undefined) {
        return;
      }

      // update (only) changed props
      for (const key of Object.keys(this.props)) {
        if (key === "children") {
          return;
        }

        if ((this.props as TProps & IndexSignature)[key] !== nextProps[key]) {
          this.element[key] = nextProps[key];
        }
      }
    }

    // tslint:disable-next-line:prefer-function-over-method
    public shouldComponentUpdate(): boolean {
      // the component shouldn't need to re-render ever because we're using it's imperative API to update
      return false;
    }

    public render(): JSX.Element {
      // we're not passing any props here because customelements can only accept strings through props
      // instead we are using the imperative javascript API to update the component
      return React.createElement(name, { ref: this.refHandler }, this.props.children);
    }
  };
}
