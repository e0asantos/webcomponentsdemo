export const pageStyle: React.CSSProperties = {
  padding: "1rem",
  fontFamily: '"Roboto", sans-serif',
  fontSize: "1rem",
  background: "#F4F6F9",
};

export const headlineStyle: React.CSSProperties = {
  marginBottom: "1rem",
  padding: ".5rem",
  fontWeight: "bold",
  fontSize: "2rem",
  color: "#a5a5a5",
  border: ".3rem #a5a5a5",
  borderStyle: "none none dotted none",
  textAlign: "center",
};

export const headerStyle: React.CSSProperties = {
  paddingTop: "1rem",
  paddingBottom: ".5rem",
  fontWeight: "bold",
  fontSize: "1.5rem",
  color: "#a5a5a5",
};

export const subHeaderStyle: React.CSSProperties = {
  paddingTop: "1rem",
  paddingBottom: ".5rem",
  fontWeight: "bold",
  fontSize: "1.2rem",
  color: "#a5a5a5",
};

export const floatStyle: React.CSSProperties = {
  float: "left",
  margin: "1rem",
};

export const breakStyle: React.CSSProperties = {
  clear: "both",
};
