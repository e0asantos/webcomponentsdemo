import * as React from "react";
import { CMXTextArea } from "../../components/textarea/component";
import { headerStyle } from "../styles";

interface ITextAreaDemoState {
  textArea1Value: string;
  textArea2Value: string;
}

export class TextAreaDemo extends React.Component<{}, ITextAreaDemoState> {
  public state = {
    textArea1Value: "",
    textArea2Value: "",
  };

  protected textArea1ChangeHandler = (event: Event) => {
    this.setState({ textArea1Value: (event.target as any).value });
  };

  protected textArea2ChangeHandler = (event: Event) => {
    this.setState({ textArea2Value: (event.target as any).value });
  };

  public render(): JSX.Element {
    return (
      <div>
        <h2 style={headerStyle}>Textarea</h2>

        <CMXTextArea
          name="textArea1"
          label="Your message"
          placeholder="Type your message here..."
          onChange={this.textArea1ChangeHandler}
          testId="TAREA-001"
        />

        <CMXTextArea
          name="textArea2"
          label="Your broken message"
          placeholder="Type your broken message here..."
          onChange={this.textArea2ChangeHandler}
          testId="TAREA-002"
          errorMessage="Your message seems broken..."
        />
      </div>
    );
  }
}
