import * as React from "react";
import { CMXTextInput } from "../../components/textinput/component";
import { headerStyle, floatStyle, breakStyle } from "../styles";

interface ITextinputDemoState {
  textinput1Value: string;
  textinput2Value: string;
  textinput3Value: string;
}

export class TextinputDemo extends React.Component<{}, ITextinputDemoState> {
  public state = {
    textinput1Value: "",
    textinput2Value: "",
    textinput3Value: "",
  };

  protected textinput1ChangeHandler = (event: Event) => {
    this.setState({ textinput1Value: (event.target as any).value });
  };

  protected textinput2ChangeHandler = (event: Event) => {
    this.setState({ textinput2Value: (event.target as any).value });
  };

  protected textinput3ChangeHandler = (event: Event) => {
    this.setState({ textinput3Value: (event.target as any).value });
  };

  public render(): JSX.Element {
    return (
      <div>
        <h2 style={headerStyle}>Text Input</h2>

        <div style={floatStyle}>
          <CMXTextInput
            name="textInput2"
            label="Firstname"
            placeholder="John"
            onChange={this.textinput1ChangeHandler}
            testId="TINPUT-001"
          />
        </div>

        <div style={floatStyle}>
          <CMXTextInput
            name="textInput2"
            label="Middlename"
            placeholder="Joseph"
            onChange={this.textinput2ChangeHandler}
            testId="TINPUT-002"
            error
          />
        </div>

        <div style={floatStyle}>
          <CMXTextInput
            name="textInput3"
            label="Surname"
            placeholder="Smith"
            onChange={this.textinput3ChangeHandler}
            testId="TINPUT-003"
            errorMessage="Computer says no!"
          />
        </div>

        <br style={breakStyle} />
      </div>
    );
  }
}
