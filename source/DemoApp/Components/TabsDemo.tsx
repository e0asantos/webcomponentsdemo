import * as React from "react";
import { CMXTabs, ICMXTab } from "../../components/tabs/component";
import { headerStyle, breakStyle } from "../styles";

interface ITabsDemoState {
  activeTab: number;
}

const tabsWrapperStyle: React.CSSProperties = {
  minWidth: "300px",
  maxWidth: "600px",
};

const tabs: ICMXTab[] = [
  { id: 1, label: "Tab 1" },
  { id: 2, label: "Tab 2" },
  { id: 3, label: "Tab 3" },
  { id: 4, label: "Tab 4" },
  { id: 5, label: "Tab 5", disabled: true },
];

export class TabsDemo extends React.Component<{}, ITabsDemoState> {
  public state = {
    activeTab: 3,
  };

  protected changeHandler = (id: number) => {
    this.setState({ activeTab: id });
  };

  public render(): JSX.Element {
    return (
      <div>
        <h2 style={headerStyle}>Tabs</h2>

        <div style={tabsWrapperStyle}>
          <CMXTabs
            tabs={tabs}
            activeTab={this.state.activeTab}
            onChange={this.changeHandler}
            testId="TABS-001"
          />
        </div>
        <br style={breakStyle} />
      </div>
    );
  }
}
