import * as React from "react";
import { CMXRadio } from "../../components/radio/component";
import { headerStyle, floatStyle, breakStyle } from "../styles";

interface IDemoAppState {
  radio1Value: string;
}

export class RadioDemo extends React.Component<{}, IDemoAppState> {
  public state = {
    radio1Value: "unknown"
  };

  protected radio1ChangeHandler = (event: Event) => {
    this.setState({ radio1Value: (event.target as any).value });
  };

  public render(): JSX.Element {
    return (
      <div>
        <h2 style={headerStyle}>Radio</h2>

        <div style={floatStyle}>
          <CMXRadio
            name="gender"
            id="GenderRadio1"
            value="male"
            label="Male"
            onChange={this.radio1ChangeHandler}
            checked={this.state.radio1Value === "male"}
            testId="RADIO-001-001"
          />
          <CMXRadio
            name="gender"
            id="GenderRadio2"
            value="female"
            label="Female"
            onChange={this.radio1ChangeHandler}
            checked={this.state.radio1Value === "female"}
            testId="RADIO-001-002"
          />
          <CMXRadio
            name="gender"
            id="GenderRadio3"
            value="unknown"
            label="Unknown"
            onChange={this.radio1ChangeHandler}
            checked={this.state.radio1Value === "unknown"}
            testId="RADIO-001-003"
          />
          <CMXRadio
            name="gender"
            id="GenderRadio4"
            value="other"
            label="Other"
            onChange={this.radio1ChangeHandler}
            checked={this.state.radio1Value === "other"}
            testId="RADIO-001-004"
            disabled
          />
        </div>
        <br style={breakStyle} />
      </div>
    );
  }
}
