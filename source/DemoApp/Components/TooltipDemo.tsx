import * as React from "react";
import { CMXTooltip } from "../../components/tooltip/component";
import { headerStyle } from "../styles";

const pStyle: React.CSSProperties = {
  padding: "1rem",
};

export class TooltipDemo extends React.Component<{}, {}> {
  public render(): JSX.Element {
    return (
      <div>
        <h2 style={headerStyle}>Tooltip</h2>

        <p style={pStyle}>
          <CMXTooltip content="I am a textual tooltip." testId="TOOLTIP-001">
            Hover over me to see a simple tooltip.
          </CMXTooltip>
        </p>

        <p style={pStyle}>
          <CMXTooltip
            content="I am <a href='https://www.cemex.com' target='_blank'>a tooltip with a link</a>."
            testId="TOOLTIP-002"
          >
            Hover over me to see a tooltip with markup.
          </CMXTooltip>
        </p>

        <p style={pStyle}>
          <CMXTooltip
            content="I am <a href='https://www.cemex.com' target='_blank'>a tooltip with a link</a> and multiple lines of text. Yes, there can be more lines of text in the tooltip..."
            testId="TOOLTIP-003"
          >
            Hover over me to see a multi-line tooltip.
          </CMXTooltip>
        </p>
      </div>
    );
  }
}
