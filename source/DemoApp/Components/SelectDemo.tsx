import * as React from "react";
import { CMXSelect } from "../../components/select/component";
import { headerStyle, floatStyle, breakStyle } from "../styles";

interface ISelectDemoState {
  select1Value: string;
  select2Value: string;
}

export class SelectDemo extends React.Component<{}, ISelectDemoState> {
  public state = {
    select1Value: "",
    select2Value: "",
  };

  protected select1ChangeHandler = (event: Event) => {
    this.setState({ select1Value: (event.target as any).value });
  };

  protected select2ChangeHandler = (event: Event) => {
    this.setState({ select2Value: (event.target as any).value });
  };

  protected select1Options: HTMLOptionElement[] = [
    new Option("Male", "male", false),
    new Option("Female", "female", false),
    new Option("Unknown", "n/a", false, true),
  ];

  protected select2Options: HTMLOptionElement[] = [
    new Option("Red", "r", false),
    new Option("Green", "g", false),
    new Option("Blue", "b", false),
    new Option("Yellow", "y", false),
    new Option("White", "w", false, true),
  ];

  public render(): JSX.Element {
    return (
      <div>
        <h2 style={headerStyle}>Select</h2>

        <div style={floatStyle}>
          <CMXSelect
            name="select1"
            label="Select gender"
            onChange={this.select1ChangeHandler}
            testId="SELECT-001"
            options={this.select1Options}
          />
        </div>

        <div style={floatStyle}>
          <CMXSelect
            name="select2"
            label="Select colour"
            onChange={this.select2ChangeHandler}
            testId="SELECT-002"
            options={this.select2Options}
            errorMessage="Computer says no!"
          />
        </div>

        <br style={breakStyle} />
      </div>
    );
  }
}
