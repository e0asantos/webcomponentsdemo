import * as React from "react";
import { CMXFooter } from "../../components/footer/component";

export class FooterDemo extends React.Component<{}, {}> {
  public render(): JSX.Element {
    return <CMXFooter />;
  }
}
