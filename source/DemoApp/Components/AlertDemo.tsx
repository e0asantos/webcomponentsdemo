import * as React from "react";
import { CMXAlert, CMXAlertType } from "../../components/alert/component";
import { headerStyle, breakStyle } from "../styles";

interface IAlertDemoState {
  alert1Closed: boolean;
  alert2Closed: boolean;
  alert3Closed: boolean;
  alert4Closed: boolean;
}

export class AlertDemo extends React.Component<{}, IAlertDemoState> {
  public state = {
    alert1Closed: false,
    alert2Closed: false,
    alert3Closed: false,
    alert4Closed: false,
  };

  protected alert1CloseHandler = () => {
    this.setState({ alert1Closed: !this.state.alert1Closed });
  };

  protected alert2CloseHandler = () => {
    this.setState({ alert2Closed: !this.state.alert2Closed });
  };

  protected alert3CloseHandler = () => {
    this.setState({ alert3Closed: !this.state.alert3Closed });
  };

  protected alert4CloseHandler = () => {
    this.setState({ alert4Closed: !this.state.alert4Closed });
  };

  public render(): JSX.Element {
    return (
      <div>
        <h2 style={headerStyle}>Alert</h2>
        {!this.state.alert1Closed && (
          <CMXAlert
            type={CMXAlertType.Error}
            message="Hey, you broke this!"
            onClose={this.alert1CloseHandler}
            testId="ALERT-001"
          />
        )}

        {!this.state.alert2Closed && (
          <CMXAlert
            type={CMXAlertType.Warning}
            message="Allright, it is getting better..."
            onClose={this.alert2CloseHandler}
            testId="ALERT-002"
          />
        )}

        {!this.state.alert3Closed && (
          <CMXAlert
            type={CMXAlertType.Info}
            message="Even better, Donald!"
            onClose={this.alert3CloseHandler}
            testId="ALERT-003"
          />
        )}

        {!this.state.alert4Closed && (
          <CMXAlert
            type={CMXAlertType.Success}
            message="Pretty cool, this is great again!"
            onClose={this.alert4CloseHandler}
            testId="ALERT-004"
          />
        )}

        <br style={breakStyle} />
      </div>
    );
  }
}
