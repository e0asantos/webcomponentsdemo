import * as React from "react";
import { CMXStepper, ICMXStep, ICMXStepType } from "../../components/stepper/component";
import { headerStyle, subHeaderStyle, breakStyle } from "../styles";

const stepperWrapperStyle: React.CSSProperties = {
  minWidth: "300px",
  maxWidth: "600px",
};

const steps1: ICMXStep[] = [
  { id: 1, label: "Step 1", type: ICMXStepType.Active },
  { id: 2, label: "Step 2", type: ICMXStepType.Inactive },
];

const steps2: ICMXStep[] = [
  { id: 1, label: "Step 1", type: ICMXStepType.Completed },
  { id: 2, label: "Step 2", type: ICMXStepType.Active },
  { id: 3, label: "Step 3", type: ICMXStepType.Inactive },
];

const steps3: ICMXStep[] = [
  { id: 1, label: "Step 1", type: ICMXStepType.Completed },
  { id: 2, label: "Step 2", type: ICMXStepType.Completed },
  { id: 3, label: "Step 3", type: ICMXStepType.Active },
  { id: 4, label: "Step 4", type: ICMXStepType.Inactive },
];

const steps4: ICMXStep[] = [
  { id: 1, label: "Step 1", type: ICMXStepType.Completed },
  { id: 2, label: "Step 2", type: ICMXStepType.Completed },
  { id: 3, label: "Step 3", type: ICMXStepType.Completed },
  { id: 4, label: "Step 4", type: ICMXStepType.Active },
  { id: 5, label: "Step 5", type: ICMXStepType.Inactive },
];

export class StepperDemo extends React.Component<{}, {}> {
  public render(): JSX.Element {
    return (
      <div>
        <h2 style={headerStyle}>Stepper</h2>

        <h3 style={subHeaderStyle}>Two points stepper</h3>
        <div style={stepperWrapperStyle}>
          <CMXStepper steps={steps1} testId="STEPPER-001" />
        </div>
        <br style={breakStyle} />

        <h3 style={subHeaderStyle}>Three points stepper</h3>
        <div style={stepperWrapperStyle}>
          <CMXStepper steps={steps2} testId="STEPPER-002" />
        </div>
        <br style={breakStyle} />

        <h3 style={subHeaderStyle}>Four points stepper</h3>
        <div style={stepperWrapperStyle}>
          <CMXStepper steps={steps3} testId="STEPPER-003" />
        </div>
        <br style={breakStyle} />

        <h3 style={subHeaderStyle}>Five points stepper</h3>
        <div style={stepperWrapperStyle}>
          <CMXStepper steps={steps4} testId="STEPPER-004" />
        </div>
        <br style={breakStyle} />
      </div>
    );
  }
}
