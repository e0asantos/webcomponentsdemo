import * as React from "react";
import { CMXPagination } from "../../components/pagination/component";
import { headerStyle, subHeaderStyle, breakStyle } from "../styles";

interface IPaginationDemoState {
  pagination1page: number;
  pagination2page: number;
  pagination3page: number;
}

const paginationWrapperStyle: React.CSSProperties = {
  minWidth: "300px",
  maxWidth: "600px",
};

export class PaginationDemo extends React.Component<{}, IPaginationDemoState> {
  public state = {
    pagination1page: 1,
    pagination2page: 10,
    pagination3page: 77,
  };

  protected pagination1Handler = (id: number) => {
    this.setState({ pagination1page: id });
  };

  protected pagination2Handler = (id: number) => {
    this.setState({ pagination2page: id });
  };

  protected pagination3Handler = (id: number) => {
    this.setState({ pagination3page: id });
  };

  public render(): JSX.Element {
    return (
      <div>
        <h2 style={headerStyle}>Pagination</h2>

        <h3 style={subHeaderStyle}>Current page in the beginning</h3>
        <div style={paginationWrapperStyle}>
          <CMXPagination
            currentPage={this.state.pagination1page}
            totalPages={12}
            onChange={this.pagination1Handler}
            testId="PAG-001"
          />
        </div>
        <br style={breakStyle} />

        <h3 style={subHeaderStyle}>Current page in the middle</h3>
        <div style={paginationWrapperStyle}>
          <CMXPagination
            currentPage={this.state.pagination2page}
            totalPages={27}
            onChange={this.pagination2Handler}
            testId="PAG-002"
          />
        </div>
        <br style={breakStyle} />

        <h3 style={subHeaderStyle}>Current page in the end</h3>
        <div style={paginationWrapperStyle}>
          <CMXPagination
            currentPage={this.state.pagination3page}
            totalPages={77}
            onChange={this.pagination3Handler}
            testId="PAG-003"
          />
        </div>
        <br style={breakStyle} />
      </div>
    );
  }
}
