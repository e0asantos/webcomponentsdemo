import * as React from "react";
import { CMXButton, ICMXButtonLayout } from "../../components/button/component";
import { headerStyle, subHeaderStyle, floatStyle, breakStyle } from "../styles";

export class ButtonDemo extends React.Component<{}, {}> {
  public render(): JSX.Element {
    return (
      <div>
        <h2 style={headerStyle}>Button</h2>

        <h3 style={subHeaderStyle}>Primary</h3>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard button"
            layout={ICMXButtonLayout.Primary}
            testId="BTN-001"
          />
        </div>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard with icon"
            icon="calendar"
            layout={ICMXButtonLayout.Primary}
            testId="BTN-002"
          />
        </div>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Solid button"
            layout={ICMXButtonLayout.PrimarySolid}
            testId="BTN-003"
          />
        </div>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Solid with icon"
            icon="calendar"
            layout={ICMXButtonLayout.PrimarySolid}
            testId="BTN-004"
          />
        </div>

        <br style={breakStyle} />

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard button"
            layout={ICMXButtonLayout.Primary}
            testId="BTN-005"
            disabled
          />
        </div>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard with icon"
            icon="calendar"
            layout={ICMXButtonLayout.Primary}
            testId="BTN-006"
            disabled
          />
        </div>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Solid button"
            layout={ICMXButtonLayout.PrimarySolid}
            testId="BTN-007"
            disabled
          />
        </div>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Solid with icon"
            icon="calendar"
            layout={ICMXButtonLayout.PrimarySolid}
            testId="BTN-008"
            disabled
          />
        </div>

        <br style={breakStyle} />

        <h3 style={subHeaderStyle}>Regular</h3>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard button"
            layout={ICMXButtonLayout.Regular}
            testId="BTN-009"
          />
        </div>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard with icon"
            icon="calendar"
            layout={ICMXButtonLayout.Regular}
            testId="BTN-010"
          />
        </div>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Solid button"
            layout={ICMXButtonLayout.RegularSolid}
            testId="BTN-011"
          />
        </div>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Solid with icon"
            icon="calendar"
            layout={ICMXButtonLayout.RegularSolid}
            testId="BTN-012"
          />
        </div>

        <br style={breakStyle} />

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard button"
            layout={ICMXButtonLayout.Regular}
            testId="BTN-013"
            disabled
          />
        </div>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard with icon"
            icon="calendar"
            layout={ICMXButtonLayout.Regular}
            testId="BTN-014"
            disabled
          />
        </div>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Solid button"
            layout={ICMXButtonLayout.RegularSolid}
            testId="BTN-015"
            disabled
          />
        </div>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Solid with icon"
            icon="calendar"
            layout={ICMXButtonLayout.RegularSolid}
            testId="BTN-016"
            disabled
          />
        </div>

        <br style={breakStyle} />

        <h3 style={subHeaderStyle}>Support</h3>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard button"
            layout={ICMXButtonLayout.Support}
            testId="BTN-017"
          />
        </div>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard with icon"
            icon="calendar"
            layout={ICMXButtonLayout.Support}
            testId="BTN-018"
          />
        </div>

        <br style={breakStyle} />

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard button"
            layout={ICMXButtonLayout.Support}
            testId="BTN-019"
            disabled
          />
        </div>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard with icon"
            icon="calendar"
            layout={ICMXButtonLayout.Support}
            testId="BTN-020"
            disabled
          />
        </div>

        <br style={breakStyle} />

        <h3 style={subHeaderStyle}>Text</h3>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard button"
            layout={ICMXButtonLayout.Text}
            testId="BTN-021"
          />
        </div>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard with icon"
            icon="calendar"
            layout={ICMXButtonLayout.Text}
            testId="BTN-022"
          />
        </div>

        <br style={breakStyle} />

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard button"
            layout={ICMXButtonLayout.Text}
            testId="BTN-023"
            disabled
          />
        </div>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard with icon"
            icon="calendar"
            layout={ICMXButtonLayout.Text}
            testId="BTN-024"
            disabled
          />
        </div>

        <br style={breakStyle} />

        <h3 style={subHeaderStyle}>Large Text</h3>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard button"
            layout={ICMXButtonLayout.LargeText}
            testId="BTN-025"
          />
        </div>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard with icon"
            icon="calendar"
            layout={ICMXButtonLayout.LargeText}
            testId="BTN-026"
          />
        </div>

        <br style={breakStyle} />

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard button"
            layout={ICMXButtonLayout.LargeText}
            testId="BTN-027"
            disabled
          />
        </div>

        <div style={floatStyle}>
          <CMXButton
            type="button"
            label="Standard with icon"
            icon="calendar"
            layout={ICMXButtonLayout.LargeText}
            testId="BTN-028"
            disabled
          />
        </div>

        <br style={breakStyle} />
      </div>
    );
  }
}
