import * as React from "react";
import { CMXStatus, CMXStatusColor, CMXStatusSize } from "../../components/status/component";
import { headerStyle, subHeaderStyle, breakStyle, floatStyle } from "../styles";

export class StatusDemo extends React.Component<{}, IAlertDemoState> {
  public render(): JSX.Element {
    return (
      <div>
        <h2 style={headerStyle}>Status</h2>

        <h3 style={subHeaderStyle}>Standard</h3>

        <div style={floatStyle}>
          <CMXStatus color={CMXStatusColor.Red} label="Failed" testId="STS-001" />
        </div>

        <div style={floatStyle}>
          <CMXStatus color={CMXStatusColor.Orange} label="In Progress" testId="STS-002" />
        </div>

        <div style={floatStyle}>
          <CMXStatus color={CMXStatusColor.Green} label="Completed" testId="STS-003" />
        </div>

        <div style={floatStyle}>
          <CMXStatus color={CMXStatusColor.Blue} label="Confirmed" testId="STS-004" />
        </div>

        <div style={floatStyle}>
          <CMXStatus color={CMXStatusColor.Grey} label="Cancelled" testId="STS-005" />
        </div>
        <br style={breakStyle} />

        <h3 style={subHeaderStyle}>Medium</h3>

        <div style={floatStyle}>
          <CMXStatus
            color={CMXStatusColor.Red}
            size={CMXStatusSize.Madium}
            label="Failed"
            testId="STS-001"
          />
        </div>

        <div style={floatStyle}>
          <CMXStatus
            color={CMXStatusColor.Orange}
            size={CMXStatusSize.Madium}
            label="In Progress"
            testId="STS-002"
          />
        </div>

        <div style={floatStyle}>
          <CMXStatus
            color={CMXStatusColor.Green}
            size={CMXStatusSize.Madium}
            label="Completed"
            testId="STS-003"
          />
        </div>

        <div style={floatStyle}>
          <CMXStatus
            color={CMXStatusColor.Blue}
            size={CMXStatusSize.Madium}
            label="Confirmed"
            testId="STS-004"
          />
        </div>

        <div style={floatStyle}>
          <CMXStatus
            color={CMXStatusColor.Grey}
            size={CMXStatusSize.Madium}
            label="Cancelled"
            testId="STS-005"
          />
        </div>
        <br style={breakStyle} />

        <h3 style={subHeaderStyle}>Small</h3>

        <div style={floatStyle}>
          <CMXStatus
            color={CMXStatusColor.Red}
            size={CMXStatusSize.Small}
            label="Failed"
            testId="STS-001"
          />
        </div>

        <div style={floatStyle}>
          <CMXStatus
            color={CMXStatusColor.Orange}
            size={CMXStatusSize.Small}
            label="In Progress"
            testId="STS-002"
          />
        </div>

        <div style={floatStyle}>
          <CMXStatus
            color={CMXStatusColor.Green}
            size={CMXStatusSize.Small}
            label="Completed"
            testId="STS-003"
          />
        </div>

        <div style={floatStyle}>
          <CMXStatus
            color={CMXStatusColor.Blue}
            size={CMXStatusSize.Small}
            label="Confirmed"
            testId="STS-004"
          />
        </div>

        <div style={floatStyle}>
          <CMXStatus
            color={CMXStatusColor.Grey}
            size={CMXStatusSize.Small}
            label="Cancelled"
            testId="STS-005"
          />
        </div>
        <br style={breakStyle} />
      </div>
    );
  }
}
