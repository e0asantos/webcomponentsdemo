import * as React from "react";
import { CMXCheckbox } from "../../components/checkbox/component";
import { headerStyle, floatStyle, breakStyle } from "../styles";

interface IDemoAppState {
  checkbox1Value: boolean;
  checkbox2Value: boolean;
  checkbox3Value: boolean;
}

export class CheckboxDemo extends React.Component<{}, IDemoAppState> {
  public state = {
    checkbox1Value: false,
    checkbox2Value: true,
    checkbox3Value: false,
  };

  protected checkbox1ChangeHandler = () => {
    this.setState({ checkbox1Value: !this.state.checkbox1Value });
  };

  protected checkbox2ChangeHandler = () => {
    this.setState({ checkbox2Value: !this.state.checkbox2Value });
  };

  protected checkbox3ChangeHandler = () => {
    this.setState({ checkbox3Value: !this.state.checkbox3Value });
  };

  public render(): JSX.Element {
    return (
      <div>
        <h2 style={headerStyle}>Checkbox</h2>

        <div style={floatStyle}>
          <CMXCheckbox
            name="checkbox1"
            label="Turn me on!"
            onChange={this.checkbox1ChangeHandler}
            checked={this.state.checkbox1Value}
            testId="CHBOX-001"
          />
        </div>

        <div style={floatStyle}>
          <CMXCheckbox
            name="checkbox2"
            label="Turn me off!"
            onChange={this.checkbox2ChangeHandler}
            checked={this.state.checkbox2Value}
            testId="CHBOX-002"
          />
        </div>

        <div style={floatStyle}>
          <CMXCheckbox
            name="checkbox3"
            label="I am disabled"
            onChange={this.checkbox3ChangeHandler}
            checked={this.state.checkbox3Value}
            disabled
            testId="CHBOX-003"
          />
        </div>

        <br style={breakStyle} />
      </div>
    );
  }
}
