import * as React from "react";
import { CMXBreadcrumbs, ICMXBreadcrumbsItem } from "../../components/breadcrumbs/component";
import { headerStyle, floatStyle, breakStyle } from "../styles";

const items: ICMXBreadcrumbsItem[] = [
  {
    label: "Control Center",
    href: "/home/control-center",
  },
  {
    label: "Customer X789548",
    onClick: (event: Event) => {
      if (confirm("Do you want to be redirected to disney.com?")) {
        document.location.href = "https://www.disney.com";
      }
    },
  },
  {
    label: "Orders",
  },
];

export class BreadcrumbsDemo extends React.Component<{}, {}> {
  public render(): JSX.Element {
    return (
      <div>
        <h2 style={headerStyle}>Breadcrumbs</h2>

        <div style={floatStyle}>
          <CMXBreadcrumbs items={items} testId="BCR-001" />
        </div>

        <br style={breakStyle} />
      </div>
    );
  }
}
