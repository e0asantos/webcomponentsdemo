import * as React from "react";

import { AlertDemo } from "./Components/AlertDemo";
import { TextinputDemo } from "./Components/TextinputDemo";
import { TextAreaDemo } from "./Components/TextAreaDemo";
import { SelectDemo } from "./Components/SelectDemo";
import { CheckboxDemo } from "./Components/CheckboxDemo";
import { RadioDemo } from "./Components/RadioDemo";
import { ButtonDemo } from "./Components/ButtonDemo";
import { TooltipDemo } from "./Components/TooltipDemo";
import { StepperDemo } from "./Components/StepperDemo";
import { TabsDemo } from "./Components/TabsDemo";
import { FooterDemo } from "./Components/FooterDemo";
import { PaginationDemo } from "./Components/PaginationDemo";
import { BreadcrumbsDemo } from "./Components/BreadcrumbsDemo";
import { StatusDemo } from "./Components/StatusDemo";

import { pageStyle, headlineStyle } from "./styles";

export class DemoApp extends React.PureComponent<{}> {
  public render(): JSX.Element {
    return (
      <div style={pageStyle}>
        <h1 style={headlineStyle}>CEMEX Web Components Demo</h1>
        <BreadcrumbsDemo />
        <PaginationDemo />
        <TabsDemo />
        <StepperDemo />
        <TooltipDemo />
        <StatusDemo />
        <AlertDemo />
        <TextinputDemo />
        <TextAreaDemo />
        <SelectDemo />
        <CheckboxDemo />
        <RadioDemo />
        <ButtonDemo />
        <FooterDemo />
      </div>
    );
  }
}
