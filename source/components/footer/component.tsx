import * as React from "jsxdom";

import { customElement } from "../../customElement";
import { createReactComponent } from "../../createReactComponent";

import * as styles from "./styles.scss";

export const CMX_FOOTER_ELEMENT_NAME = "cmx-footer";

export interface ICMXFooterElement {}

@customElement(CMX_FOOTER_ELEMENT_NAME)
export class CMXFooterElement extends HTMLElement implements ICMXFooterElement {
  public connectedCallback(): void {
    const year = new Date().getFullYear();
    this.appendChild(
      <footer role="contentinfo" className={styles.footer}>
        <p>{`Copyright \u00A9 ${year}, CEMEX International Holding AG. All rights reserved.`}</p>
        <nav aria-label="legal links" role="navigation">
          <ul>
            <li>
              <a href="/public/legal">Legal</a>
            </li>
            <li>
              <a href="https://www.cemex.com/privacy-policy" target="_blank">
                Privacy
              </a>
            </li>
            <li>
              <a href="https://www.cemex.com/" target="_blank">
                cemex.com
              </a>
            </li>
          </ul>
        </nav>
      </footer>,
    );
  }
}

export const CMXFooter = createReactComponent<ICMXFooterElement>(CMX_FOOTER_ELEMENT_NAME);
