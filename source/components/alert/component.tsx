import * as React from "jsxdom";

import { customElement } from "../../customElement";
import { createReactComponent } from "../../createReactComponent";

import { testAttrBaseName } from "../../constants";

import * as styles from "./styles.scss";

export const CMX_ALERT_ELEMENT_NAME = "cmx-alert";

export enum CMXAlertType {
  Error = "error",
  Info = "info",
  Success = "success",
  Warning = "warning",
}

export interface ICMXAlertElement {
  message: string;
  onClose: typeof HTMLDivElement.prototype.onclick;
  testId: string;
  type: CMXAlertType;
}

@customElement(CMX_ALERT_ELEMENT_NAME)
export class CMXAlertElement extends HTMLElement implements ICMXAlertElement {
  protected wrapperElement: HTMLDivElement;
  protected alertElement: HTMLInputElement;
  protected closeElement: HTMLDivElement;

  public set message(value: string) {
    this.alertElement.innerText = value;
  }

  public set onClose(value: () => void) {
    this.closeElement.onclick = value;
  }

  public set testId(value: string) {
    this.alertElement.dataset[testAttrBaseName] = `${value}-message`;
    this.closeElement.dataset[testAttrBaseName] = `${value}-close`;
    this.wrapperElement.dataset[testAttrBaseName] = `${value}`;
  }

  public set type(value: CMXAlertType) {
    this.wrapperElement.classList.add(styles[`alert--${value}`]);
  }

  public connectedCallback(): void {
    this.appendChild(
      <div className={styles.alert} ref={element => (this.wrapperElement = element)}>
        <div
          className={styles.alert__text}
          ref={element => (this.alertElement = element)}
          role="alert"
        />
        <div className={styles.alert__close} ref={element => (this.closeElement = element)}>
          <i className={[styles.alert__icon, styles["cmx-icon-close"]].join(" ")} />
        </div>
      </div>,
    );
  }
}

export const CMXAlert = createReactComponent<ICMXAlertElement>(CMX_ALERT_ELEMENT_NAME);
