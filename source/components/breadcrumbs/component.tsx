import * as React from "jsxdom";

import { customElement } from "../../customElement";
import { createReactComponent } from "../../createReactComponent";

import { testAttrBaseName } from "../../constants";

import * as styles from "./styles.scss";

export const CMX_BREADCRUMBS_ELEMENT_NAME = "cmx-breadcrumbs";

export interface ICMXBreadcrumbsItem {
  label: string;
  href?: string;
  onClick?: typeof HTMLAnchorElement.prototype.onclick;
}

export interface ICMXBreadcrumbsElement {
  items: ICMXBreadcrumbsItem[];
  testId: string;
}

@customElement(CMX_BREADCRUMBS_ELEMENT_NAME)
export class CMXBreadcrumbsElement extends HTMLElement implements ICMXBreadcrumbsElement {
  protected listElement: HTMLUListElement;

  public set items(items: ICMXBreadcrumbsItem[]) {
    items.map((item, index) => {
      const level = index + 1;
      this.listElement.appendChild(
        <li className={styles.breadcrumb__item}>
          {item.href || item.onClick ? (
            <a
              className={styles.breadcrumb__link}
              href={item.href ? item.href : "#"}
              onclick={item.onClick && item.onClick}
              aria-level={level}
            >
              {item.label}
            </a>
          ) : (
            <span aria-current="true" aria-level={level}>
              {item.label}
            </span>
          )}
        </li>,
      );
    });
  }

  public set testId(value: string) {
    this.listElement.dataset[testAttrBaseName] = `${value}`;
  }

  public connectedCallback(): void {
    this.appendChild(
      <nav aria-label="breadcrumb" role="navigation">
        <ul className={styles.breadcrumb} ref={element => (this.listElement = element)} />
      </nav>,
    );
  }
}

export const CMXBreadcrumbs = createReactComponent<ICMXBreadcrumbsElement>(
  CMX_BREADCRUMBS_ELEMENT_NAME,
);
