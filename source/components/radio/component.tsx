import * as React from "jsxdom";

import { customElement } from "../../customElement";
import { createReactComponent } from "../../createReactComponent";

import { testAttrBaseName } from "../../constants";

import * as styles from "./styles.scss";

export const CMX_RADIO_ELEMENT_NAME = "cmx-radio";

export interface ICMXRadioElement {
  id: string;
  name: string;
  label: string;
  checked: boolean;
  disabled?: boolean;
  onChange: typeof HTMLInputElement.prototype.onchange;
  testId: string;
  value: string;
}

@customElement(CMX_RADIO_ELEMENT_NAME)
export class CMXRadioElement extends HTMLElement implements ICMXRadioElement {
  protected radioElement: HTMLInputElement;
  protected labelElement: HTMLLabelElement;
  protected labelContentElement: HTMLSpanElement;

  public set checked(value: boolean) {
    if (!this.radioElement.disabled) {
      this.radioElement.checked = value;
      this.radioElement.setAttribute("aria-checked", value.toString());
    }
  }

  public set disabled(value: boolean) {
    this.radioElement.disabled = value;
  }

  public set id(value: string) {
    this.radioElement.id = value;
    this.labelElement.htmlFor = value;
  }

  public set label(value: string) {
    this.labelContentElement.innerText = value;
  }

  public set name(value: string) {
    this.radioElement.name = value;
  }

  public set onChange(changeHandler: typeof HTMLInputElement.prototype.onchange) {
    this.radioElement.onchange = changeHandler;
  }

  public set testId(value: string) {
    this.radioElement.dataset[testAttrBaseName] = `${value}-input`;
    this.labelElement.dataset[testAttrBaseName] = `${value}-label`;
    this.dataset[testAttrBaseName] = `${value}`;
  }

  public set value(value: string) {
    this.radioElement.value = value;
  }

  public connectedCallback(): void {
    this.className = styles.radio;

    this.appendChild(
      <input
        role="radio"
        type="radio"
        className={styles.radio__element}
        ref={element => (this.radioElement = element)}
      />,
    );

    this.appendChild(
      <label className={styles.radio__label} ref={element => (this.labelElement = element)}>
        <span
          className={styles["radio__label-content"]}
          ref={element => (this.labelContentElement = element)}
        />
      </label>,
    );
  }
}

export const CMXRadio = createReactComponent<ICMXRadioElement>(CMX_RADIO_ELEMENT_NAME);
