import * as React from "jsxdom";

import { customElement } from "../../customElement";
import { createReactComponent } from "../../createReactComponent";

import { testAttrBaseName } from "../../constants";

import * as styles from "./styles.scss";

export const CMX_PAGINATION_ELEMENT_NAME = "cmx-pagination";

const defaultGroupSize = 3;

export interface ICMXPaginationElement {
  currentPage: number;
  totalPages: number;
  groupSize?: number;
  onChange(id: number): void;
  testId: string;
}

@customElement(CMX_PAGINATION_ELEMENT_NAME)
export class CMXPaginationElement extends HTMLElement implements ICMXPaginationElement {
  protected listElement: HTMLUListElement;
  protected _currentPage: number;
  protected _totalPages: number;
  protected _groupSize: number = defaultGroupSize;
  protected _changeHandler: (id: number) => void;
  protected _testId: string;

  private setPage(id: number): void {
    if (id > 0 && id <= this._totalPages) {
      this._changeHandler(id);
    }
  }

  private updateView(): void {
    this.listElement.innerHTML = "";

    const groupSize = this._groupSize;
    const groupSizeHalf = Math.floor(groupSize / 2);
    const showLeadingArrow = this._currentPage !== 1;
    const showTrailingArrow = this._currentPage !== this._totalPages;

    let start = 1;
    let finish = this._totalPages;
    let showFirstPageLink = false;
    let showLastPageLink = false;

    // Current page is in the leading group of numbers
    if (this._currentPage < groupSize) {
      finish = groupSize;
      if (this._totalPages > groupSize + 1) {
        showLastPageLink = true;
      }
    }

    // Current page is in the middle group of numbers
    if (this._currentPage >= groupSize && this._currentPage <= this._totalPages - groupSize + 1) {
      start = this._currentPage - groupSizeHalf;
      finish = this._currentPage + groupSizeHalf;
      showFirstPageLink = true;
      showLastPageLink = true;
    }

    // Current page is in the trailing group of numbers
    if (this._currentPage > this._totalPages - groupSize + 1) {
      start = this._totalPages - groupSize + 1;
      showFirstPageLink = true;
    }

    // Assign testId to root element
    this.listElement.dataset[testAttrBaseName] = `${this._testId}`;

    if (showLeadingArrow) {
      this.listElement.appendChild(
        <li className={[styles.pagination__item, styles["pagination__item--control"]].join(" ")}>
          <a
            onclick={() => this.setPage(this._currentPage - 1)}
            className={styles["pagination__item-elem"]}
            aria-label="Goto Previous Page"
          >
            <i className={styles["cmx-icon-straight-arrow-left"]} />
          </a>
        </li>,
      );
    }

    if (showFirstPageLink) {
      this.listElement.appendChild(
        <li className={styles.pagination__item}>
          <a
            className={styles["pagination__item-elem"]}
            onclick={() => this.setPage(1)}
            aria-label="Goto First Page"
          >
            1
          </a>
        </li>,
      );
      this.listElement.appendChild(
        <li className={styles.pagination__item} aria-hidden="true">
          <span className={styles["pagination__item-dots"]}>...</span>
        </li>,
      );
    }

    for (let page = start; page <= finish; page++) {
      this.listElement.appendChild(
        <li className={styles.pagination__item}>
          <a
            className={[
              styles["pagination__item-elem"],
              page === this._currentPage && styles["pagination__item-elem--active"],
            ]
              .filter(Boolean)
              .join(" ")}
            onclick={() => this.setPage(page)}
            aria-current={page === this._currentPage}
            aria-label={`Goto Page ${page}`}
          >
            {page}
          </a>
        </li>,
      );
    }

    if (showLastPageLink) {
      this.listElement.appendChild(
        <li className={styles.pagination__item} aria-hidden="true">
          <span className={styles["pagination__item-dots"]}>...</span>
        </li>,
      );
      this.listElement.appendChild(
        <li className={styles.pagination__item}>
          <a
            className={styles["pagination__item-elem"]}
            onclick={() => this.setPage(this._totalPages)}
            aria-label="Goto Last Page"
          >
            {this._totalPages}
          </a>
        </li>,
      );
    }

    if (showTrailingArrow) {
      this.listElement.appendChild(
        <li className={[styles.pagination__item, styles["pagination__item--control"]].join(" ")}>
          <a
            onclick={() => this.setPage(this._currentPage + 1)}
            className={styles["pagination__item-elem"]}
            aria-label="Goto Next Page"
          >
            <i className={styles["cmx-icon-straight-arrow-right"]} />
          </a>
        </li>,
      );
    }
  }

  public set currentPage(value: number) {
    this._currentPage = value;
    this.updateView();
  }

  public set totalPages(value: number) {
    this._totalPages = value;
    this.updateView();
  }

  public set groupSize(value: number) {
    this._groupSize = value;
    this.updateView();
  }

  public set onChange(changeHandler: (id: number) => void) {
    this._changeHandler = changeHandler;
    this.updateView();
  }

  public set testId(value: string) {
    this._testId = value;
    this.updateView();
  }

  public connectedCallback(): void {
    this.appendChild(
      <nav role="navigation" aria-label="Pagination Navigation">
        <ul className={styles.pagination} ref={element => (this.listElement = element)} />
      </nav>,
    );
  }
}

export const CMXPagination = createReactComponent<ICMXPaginationElement>(
  CMX_PAGINATION_ELEMENT_NAME,
);
