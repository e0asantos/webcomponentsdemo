import * as React from "jsxdom";

import { customElement } from "../../customElement";
import { createReactComponent } from "../../createReactComponent";

import { testAttrBaseName } from "../../constants";

import * as styles from "./styles.scss";

export const CMX_TOOLTIP_ELEMENT_NAME = "cmx-tooltip";

export interface ICMXTooltipElement {
  content: HTMLElement | string;
  testId: string;
}

@customElement(CMX_TOOLTIP_ELEMENT_NAME)
export class CMXTooltipElement extends HTMLElement implements ICMXTooltipElement {
  protected tooltipElement: HTMLDivElement;

  public set content(value: HTMLElement | string) {
    if (value instanceof HTMLElement) {
      this.tooltipElement.appendChild(value);
    } else {
      this.tooltipElement.innerHTML = value;
    }
  }

  public set testId(value: string) {
    this.dataset[testAttrBaseName] = `${value}-wrapper`;
    this.tooltipElement.dataset[testAttrBaseName] = `${value}-content`;
  }

  public connectedCallback(): void {
    this.className = styles.tooltip__wrapper;

    this.appendChild(
      <div
        role="tooltip"
        className={styles.tooltip__element}
        ref={element => (this.tooltipElement = element)}
      />,
    );
  }
}

export const CMXTooltip = createReactComponent<ICMXTooltipElement>(CMX_TOOLTIP_ELEMENT_NAME);
