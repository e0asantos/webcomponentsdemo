import * as React from "jsxdom";

import { customElement } from "../../customElement";
import { createReactComponent } from "../../createReactComponent";

import { testAttrBaseName } from "../../constants";

import * as styles from "./styles.scss";

export const CMX_TEXTINPUT_ELEMENT_NAME = "cmx-textinput";

export interface ICMXTextInputElement {
  autofocus?: boolean;
  error?: boolean;
  errorMessage?: string;
  label?: string;
  name: string;
  onChange: typeof HTMLInputElement.prototype.onchange;
  placeholder?: string;
  testId: string;
  value?: string;
}

@customElement(CMX_TEXTINPUT_ELEMENT_NAME)
export class CMXTextInputElement extends HTMLElement implements ICMXTextInputElement {
  protected labelElement: HTMLLabelElement;
  protected inputElement: HTMLInputElement;
  protected errorMessageElement: HTMLDivElement;

  public set autofocus(value: boolean) {
    this.inputElement.autofocus = value;
  }

  public set error(value: boolean) {
    if (value) {
      this.inputElement.classList.add(styles["input__element--error"]);
    } else {
      this.inputElement.classList.remove(styles["input__element--error"]);
    }
  }

  public set errorMessage(value: string) {
    this.errorMessageElement.innerText = value;
    this.error = true;
  }

  public set label(value: string) {
    this.labelElement.innerText = value;
  }

  public set name(value: string) {
    this.inputElement.name = value;
    this.inputElement.id = value;
    this.labelElement.htmlFor = value;
  }

  public set onChange(changeHandler: typeof HTMLInputElement.prototype.onchange) {
    // TODO: test this
    this.inputElement.onkeyup = changeHandler;
  }

  public set placeholder(value: string) {
    this.inputElement.placeholder = value;
  }

  public set testId(value: string) {
    this.inputElement.dataset[testAttrBaseName] = `${value}-input`;
    this.labelElement.dataset[testAttrBaseName] = `${value}-label`;
    this.dataset[testAttrBaseName] = `${value}`;
  }

  public set value(value: string) {
    this.inputElement.value = value;
  }

  public connectedCallback(): void {
    this.className = styles.input;

    this.appendChild(
      <label className={styles.input__label} ref={element => (this.labelElement = element)} />,
    );

    this.appendChild(
      <input
        role="textbox"
        type="text"
        className={styles.input__element}
        ref={element => (this.inputElement = element)}
      />,
    );

    this.appendChild(
      <div
        className={styles["input__error-message"]}
        ref={element => (this.errorMessageElement = element)}
      />,
    );
  }
}

export const CMXTextInput = createReactComponent<ICMXTextInputElement>(CMX_TEXTINPUT_ELEMENT_NAME);
