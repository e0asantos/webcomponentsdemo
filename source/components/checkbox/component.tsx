import * as React from "jsxdom";

import { customElement } from "../../customElement";
import { createReactComponent } from "../../createReactComponent";

import { testAttrBaseName } from "../../constants";

import * as styles from "./styles.scss";

export const CMX_CHECKBOX_ELEMENT_NAME = "cmx-checkbox";

export interface ICMXCheckboxElement {
  name: string;
  label: string;
  checked: boolean;
  disabled?: boolean;
  onChange: typeof HTMLInputElement.prototype.onclick;
  testId: string;
}

@customElement(CMX_CHECKBOX_ELEMENT_NAME)
export class CMXCheckboxElement extends HTMLElement implements ICMXCheckboxElement {
  protected checkboxElement: HTMLInputElement;
  protected labelElement: HTMLLabelElement;
  protected labelContentElement: HTMLSpanElement;

  public set checked(value: boolean) {
    if (!this.checkboxElement.disabled) {
      this.checkboxElement.checked = value;
      this.checkboxElement.setAttribute("aria-checked", value.toString());
    }
  }

  public set disabled(value: boolean) {
    this.checkboxElement.disabled = value;
  }

  public set label(value: string) {
    this.labelContentElement.innerText = value;
  }

  public set name(value: string) {
    this.checkboxElement.name = value;
    this.labelElement.htmlFor = value;
  }

  public set onChange(changeHandler: () => void) {
    this.onclick = () => {
      if (!this.checkboxElement.disabled) {
        changeHandler();
      }
    };
  }

  public set testId(value: string) {
    this.checkboxElement.dataset[testAttrBaseName] = `${value}-input`;
    this.labelElement.dataset[testAttrBaseName] = `${value}-label`;
    this.dataset[testAttrBaseName] = `${value}`;
  }

  public connectedCallback(): void {
    this.className = styles.checkbox;

    this.appendChild(
      <input
        role="checkbox"
        type="checkbox"
        className={styles.checkbox__input}
        ref={element => (this.checkboxElement = element)}
      />,
    );

    this.appendChild(
      <label className={styles.checkbox__label} ref={element => (this.labelElement = element)}>
        <span
          className={styles["checkbox__label-content"]}
          ref={element => (this.labelContentElement = element)}
        />
      </label>,
    );
  }
}

export const CMXCheckbox = createReactComponent<ICMXCheckboxElement>(CMX_CHECKBOX_ELEMENT_NAME);
