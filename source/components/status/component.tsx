import * as React from "jsxdom";

import { customElement } from "../../customElement";
import { createReactComponent } from "../../createReactComponent";

import { testAttrBaseName } from "../../constants";

import * as styles from "./styles.scss";

export const CMX_STATUS_ELEMENT_NAME = "cmx-status";

export enum CMXStatusSize {
  Standard = "standard",
  Madium = "medium",
  Small = "small",
}

export enum CMXStatusColor {
  Red = "red",
  Green = "green",
  Orange = "orange",
  Grey = "grey",
  Blue = "blue",
}

export interface ICMXStatusElement {
  label: string;
  testId: string;
  size?: CMXStatusSize;
  color?: CMXStatusColor;
}

const defaultSize = CMXStatusSize.Standard;
const defaultColor = CMXStatusColor.Blue;

@customElement(CMX_STATUS_ELEMENT_NAME)
export class CMXStatusElement extends HTMLElement implements ICMXStatusElement {
  protected boxElement: HTMLDivElement;
  protected labelElement: HTMLSpanElement;
  protected _label: string;
  protected _color: CMXStatusColor = defaultColor;
  protected _size: CMXStatusSize = defaultSize;

  protected updateView() {
    this.boxElement.className = [
      styles.status__box,
      styles[`status__box--${this._size}`],
      styles[`status__box--${this._color}`],
    ].join(" ");

    this.labelElement.className = [
      styles.status__label,
      styles[`status__label--${this._size}`],
    ].join(" ");

    this.labelElement.innerText = this._label;
  }

  public set label(value: string) {
    this._label = value;
    this.updateView();
  }

  public set testId(value: string) {
    this.dataset[testAttrBaseName] = `${value}`;
  }

  public set size(value: CMXStatusSize) {
    this._size = value;
    this.updateView();
  }

  public set color(value: CMXStatusColor) {
    this._color = value;
    this.updateView();
  }

  public connectedCallback(): void {
    this.className = styles.status;

    this.appendChild(<div ref={element => (this.boxElement = element)} />);

    this.appendChild(<span role="status" ref={element => (this.labelElement = element)} />);
  }
}

export const CMXStatus = createReactComponent<ICMXStatusElement>(CMX_STATUS_ELEMENT_NAME);
