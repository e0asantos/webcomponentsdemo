import * as React from "jsxdom";

import { customElement } from "../../customElement";
import { createReactComponent } from "../../createReactComponent";

import { testAttrBaseName } from "../../constants";

import * as styles from "./styles.scss";

export const CMX_STEPPER_ELEMENT_NAME = "cmx-stepper";

export enum ICMXStepType {
  Completed = "completed",
  Active = "active",
  Inactive = "inactive",
}

export interface ICMXStep {
  id: number;
  label: string;
  type: ICMXStepType;
}

export interface ICMXStepperElement {
  steps: ICMXStep[];
  testId: string;
}

@customElement(CMX_STEPPER_ELEMENT_NAME)
export class CMXStepperElement extends HTMLElement implements ICMXStepperElement {
  protected progressElement: HTMLDivElement;

  public set steps(steps: ICMXStep[]) {
    const numSteps = steps.length;
    const stepWidth = 100 / numSteps;
    const progressWidth = stepWidth * numSteps - stepWidth;
    const progressMargin = (100 - progressWidth) / 2;
    const stepStyle = `width: ${stepWidth.toFixed(2)}%`;

    this.progressElement.style.cssText = `width: ${progressWidth.toFixed(
      2,
    )}%; margin-left:${progressMargin.toFixed(2)}%`;
    steps.map(step => {
      this.appendChild(
        <div
          role="tab"
          aria-selected={step.type === ICMXStepType.Active}
          className={[styles.step, styles[`step--${step.type}`]].join(" ")}
          style={stepStyle}
        >
          <div className={styles["step-icon"]}>
            {step.type === ICMXStepType.Completed ? (
              <i className={styles["cmx-icon-check"]} />
            ) : (
              step.id
            )}
          </div>
          <label className={styles["step-label"]}>{step.label}</label>
        </div>,
      );
    });
  }

  public set testId(value: string) {
    this.dataset[testAttrBaseName] = `${value}`;
  }

  public connectedCallback(): void {
    this.className = styles.stepper;
    this.appendChild(
      <div
        role="tablist"
        className={styles.progress}
        ref={element => (this.progressElement = element)}
      />,
    );
  }
}

export const CMXStepper = createReactComponent<ICMXStepperElement>(CMX_STEPPER_ELEMENT_NAME);
