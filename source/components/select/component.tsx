import * as React from "jsxdom";

import { customElement } from "../../customElement";
import { createReactComponent } from "../../createReactComponent";

import { testAttrBaseName } from "../../constants";

import * as styles from "./styles.scss";

export const CMX_SELECT_ELEMENT_NAME = "cmx-select";

// TODO: extend common Form (autofocus, ...), Component (testId, ...) etc. properties
export interface ICMXSelectElement {
  autofocus?: boolean;
  error?: boolean;
  errorMessage?: string;
  label?: string;
  multiple?: boolean;
  name: string;
  onChange: typeof HTMLSelectElement.prototype.onchange;
  options?: HTMLOptionElement[];
  size?: number;
  testId: string;
}

@customElement(CMX_SELECT_ELEMENT_NAME)
export class CMXSelectElement extends HTMLElement implements ICMXSelectElement {
  protected labelElement: HTMLLabelElement;
  protected selectWrapperElement: HTMLDivElement;
  protected selectElement: HTMLSelectElement;
  protected errorMessageElement: HTMLDivElement;

  public set autofocus(value: boolean) {
    this.selectElement.autofocus = value;
  }

  public set error(value: boolean) {
    if (value) {
      this.selectWrapperElement.classList.add(styles["select__element--error"]);
    } else {
      this.selectWrapperElement.classList.remove(styles["select__element--error"]);
    }
  }

  public set errorMessage(value: string) {
    this.errorMessageElement.innerText = value;
    this.error = true;
  }

  public set label(value: string) {
    this.labelElement.innerText = value;
  }

  public set multiple(value: boolean) {
    this.selectElement.multiple = value;
  }

  public set name(value: string) {
    this.selectElement.name = value;
    this.labelElement.htmlFor = value;
  }

  public set onChange(changeHandler: typeof HTMLSelectElement.prototype.onchange) {
    this.selectElement.onchange = changeHandler;
  }

  public set options(options: HTMLOptionElement[]) {
    options.map(option => {
      this.selectElement.options.add(option);
    });
  }

  public set size(value: number) {
    this.selectElement.size = value;
  }

  public set testId(value: string) {
    this.selectElement.dataset[testAttrBaseName] = `${value}-input`;
    this.labelElement.dataset[testAttrBaseName] = `${value}-label`;
    this.dataset[testAttrBaseName] = `${value}`;
  }

  public connectedCallback(): void {
    this.className = styles.select;

    this.appendChild(
      <label className={styles.select__label} ref={element => (this.labelElement = element)} />,
    );

    this.appendChild(
      <div
        className={styles.select__element}
        ref={element => (this.selectWrapperElement = element)}
      >
        <select
          role="textbox"
          aria-label="Select"
          className={styles["select__element-select"]}
          ref={element => (this.selectElement = element)}
        />
      </div>,
    );

    this.appendChild(
      <div
        className={styles["select__error-message"]}
        ref={element => (this.errorMessageElement = element)}
      />,
    );
  }
}

export const CMXSelect = createReactComponent<ICMXSelectElement>(CMX_SELECT_ELEMENT_NAME);
