import * as React from "jsxdom";

import { customElement } from "../../customElement";
import { createReactComponent } from "../../createReactComponent";

import { testAttrBaseName } from "../../constants";

import * as styles from "./styles.scss";

export const CMX_TABS_ELEMENT_NAME = "cmx-tabs";

export interface ICMXTab {
  id: number;
  label: string;
  disabled?: boolean;
}

export interface ICMXTabsElement {
  tabs: ICMXTab[];
  activeTab: number;
  onChange(id: number): void;
  testId: string;
}

// TODO: consider adding tab content eg.
// <a id="foo-tab" href="#foo" role="tab" aria-controls="foo" ...
// <section id="foo" role="tabpanel" aria-labelledby="foo-tab">
// ...
// </section>
@customElement(CMX_TABS_ELEMENT_NAME)
export class CMXTabsElement extends HTMLElement implements ICMXTabsElement {
  protected listElement: HTMLUListElement;
  protected itemElements: HTMLLIElement[] = [];
  protected linkElements: HTMLAnchorElement[] = [];
  protected _tabs: ICMXTab[];
  protected _activeTab: number;
  protected _changeHandler: (id: number) => void;
  protected _testId: string;

  protected updateView() {
    this.listElement.dataset[testAttrBaseName] = `${this._testId}-tabs`;

    this._tabs &&
      this._tabs.map((tab, index) => {
        this.itemElements[index].setAttribute("role", "presentation");

        if (this._activeTab !== tab.id && !tab.disabled && this._changeHandler) {
          this.itemElements[index].onclick = () => this._changeHandler(tab.id);
        }

        if (this._testId) {
          this.itemElements[index].dataset[testAttrBaseName] = `${this._testId}-tab-${tab.id}`;
          this.linkElements[index].dataset[testAttrBaseName] = `${this._testId}-tab-link-${tab.id}`;
        }

        if (this._activeTab === tab.id) {
          this.itemElements[index].setAttribute("aria-selected", "true");
          this.linkElements[index].className = [
            styles["tablist__tab-link"],
            styles["tablist__tab-link--active"],
          ].join(" ");
        } else if (tab.disabled) {
          this.itemElements[index].setAttribute("aria-hidden", "true");
          this.linkElements[index].className = [
            styles["tablist__tab-link"],
            styles["tablist__tab-link--disabled"],
          ].join(" ");
        } else {
          this.linkElements[index].className = styles["tablist__tab-link"];
        }
      });
  }

  public set activeTab(value: number) {
    this._activeTab = value;
    this.updateView();
  }

  public set onChange(changeHandler: (id: number) => void) {
    this._changeHandler = changeHandler;
    this.updateView();
  }

  public set tabs(tabs: ICMXTab[]) {
    this._tabs = tabs;
    this.listElement.innerHTML = "";
    this._tabs.map(tab => {
      this.listElement.appendChild(
        <li
          role="tab"
          ref={element => this.itemElements.push(element)}
          className={styles["tablist__tab"]}
        >
          <a ref={element => this.linkElements.push(element)}>{tab.label}</a>
        </li>,
      );
    });
  }

  public set testId(value: string) {
    this._testId = value;
    this.updateView();
  }

  public connectedCallback(): void {
    this.appendChild(
      <ul
        role="tablist"
        className={styles.tablist}
        ref={element => (this.listElement = element)}
      />,
    );
  }
}

export const CMXTabs = createReactComponent<ICMXTabsElement>(CMX_TABS_ELEMENT_NAME);
