import * as React from "jsxdom";

import { customElement } from "../../customElement";
import { createReactComponent } from "../../createReactComponent";

import { testAttrBaseName } from "../../constants";

import * as styles from "./styles.scss";

export const CMX_TEXTAREA_ELEMENT_NAME = "cmx-textarea";

export interface ICMXTextAreaElement {
  autofocus?: boolean;
  error?: boolean;
  errorMessage?: string;
  label?: string;
  name: string;
  onChange: typeof HTMLTextAreaElement.prototype.onchange;
  placeholder?: string;
  testId: string;
  value?: string;
}

@customElement(CMX_TEXTAREA_ELEMENT_NAME)
export class CMXTextAreaElement extends HTMLElement implements ICMXTextAreaElement {
  protected labelElement: HTMLLabelElement;
  protected textAreaElement: HTMLTextAreaElement;
  protected errorMessageElement: HTMLDivElement;

  public set autofocus(value: boolean) {
    this.textAreaElement.autofocus = value;
  }

  public set error(value: boolean) {
    if (value) {
      this.textAreaElement.classList.add(styles["textarea__element--error"]);
    } else {
      this.textAreaElement.classList.remove(styles["textarea__element--error"]);
    }
  }

  public set errorMessage(value: string) {
    this.errorMessageElement.innerText = value;
    this.error = true;
  }

  public set label(value: string) {
    this.labelElement.innerText = value;
  }

  public set name(value: string) {
    this.textAreaElement.name = value;
    this.textAreaElement.id = value;
    this.labelElement.htmlFor = value;
  }

  public set onChange(changeHandler: typeof HTMLTextAreaElement.prototype.onchange) {
    // TODO: test this
    this.textAreaElement.onkeyup = changeHandler;
  }

  public set placeholder(value: string) {
    this.textAreaElement.placeholder = value;
  }

  public set testId(value: string) {
    this.textAreaElement.dataset[testAttrBaseName] = `${value}-input`;
    this.labelElement.dataset[testAttrBaseName] = `${value}-label`;
    this.dataset[testAttrBaseName] = `${value}`;
  }

  public set value(value: string) {
    this.textAreaElement.value = value;
  }

  public connectedCallback(): void {
    this.className = styles.input;

    this.appendChild(
      <label className={styles.textarea__label} ref={element => (this.labelElement = element)} />,
    );

    this.appendChild(
      <textarea
        role="textbox"
        className={styles.textarea__element}
        ref={element => (this.textAreaElement = element)}
      />,
    );

    this.appendChild(
      <div
        className={styles["textarea__error-message"]}
        ref={element => (this.errorMessageElement = element)}
      />,
    );
  }
}

export const CMXTextArea = createReactComponent<ICMXTextAreaElement>(CMX_TEXTAREA_ELEMENT_NAME);
