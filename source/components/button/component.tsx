import * as React from "jsxdom";

import { customElement } from "../../customElement";
import { createReactComponent } from "../../createReactComponent";

import { testAttrBaseName } from "../../constants";

import * as styles from "./styles.scss";

export const CMX_BUTTON_ELEMENT_NAME = "cmx-button";

export interface ICMXButtonElement {
  layout: ICMXButtonLayout;
  disabled?: boolean;
  icon?: string;
  label: string;
  onClick?: typeof HTMLButtonElement.prototype.onclick;
  testId: string;
  type: typeof HTMLButtonElement.prototype.type;
}

export const enum ICMXButtonLayout {
  Primary = "primary",
  PrimarySolid = "primary-solid",
  Regular = "regular",
  RegularSolid = "regular-solid",
  Support = "support",
  SupportSolid = "support-solid",
  Text = "text",
  LargeText = "text-large",
}

@customElement(CMX_BUTTON_ELEMENT_NAME)
export class CMXButtonElement extends HTMLElement implements ICMXButtonElement {
  protected buttonElement: HTMLButtonElement;
  protected labelElement: HTMLSpanElement;

  public set layout(value: ICMXButtonLayout) {
    this.buttonElement.classList.add(styles[`button--${value}`]);
  }

  public set disabled(value: boolean) {
    this.buttonElement.disabled = value;
  }

  public set icon(value: string) {
    this.labelElement.insertBefore(
      <i
        className={[styles.button__icon, styles[`cmx-icon-${value}`]].join(" ")}
        aria-hidden="true"
      />,
      this.labelElement.childNodes[0],
    );
  }

  public set label(value: string) {
    this.buttonElement.appendChild(
      <span ref={element => (this.labelElement = element)}>{value}</span>,
    );
  }

  public set onClick(clickHandler: () => void) {
    this.buttonElement.onclick = clickHandler;
  }

  public set testId(value: string) {
    this.buttonElement.dataset[testAttrBaseName] = value;
  }

  public set type(value: typeof HTMLButtonElement.prototype.type) {
    this.buttonElement.type = value;
  }

  public connectedCallback(): void {
    this.appendChild(
      <button className={styles.button} ref={element => (this.buttonElement = element)} />,
    );
  }
}

export const CMXButton = createReactComponent<ICMXButtonElement>(CMX_BUTTON_ELEMENT_NAME);
